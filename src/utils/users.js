const users = [];

const addUser = ({ id, username }) => {
  // Clean the data
  username = username.trim().toLowerCase();
  room = "chatRoom";

  // Validate the data
  if (!username) {
    return {
      error: "Username is required!",
    };
  }

  // Check for existing user
  const existingUser = users.find((user) => {
    return user.username === username;
  });

  // Validate username
  if (existingUser) {
    return {
      error: "Username is in use!",
    };
  }

  // Store user
  const user = { id, username, room };
  users.push(user);
  return { user };
};

const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};

const getUser = (id) => {
  return users.find((user) => user.id === id);
};

const getUsersInRoom = (user) => {
  console.log(user, users);
  room = "chatRoom";
  return users.slice();
};

module.exports = {
  addUser,
  removeUser,
  getUser,
  getUsersInRoom,
};
